import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
    	AStarSearch search;
	    Board b = new Board(args);
	    ArrayList<Board> moveSequence;

	    search = new AStarSearch(b.getBoard().length);

	    moveSequence = search.search(b);

	    if(moveSequence != null) {
			for(Board move : moveSequence) {
				System.out.println(move);
			}
		} else {
	    	System.out.println("Something went wrong! The search function returned null...");
		}
    }
}
