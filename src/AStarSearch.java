import java.util.*;

public class AStarSearch {
    private int[][] mGoalState;
    private HashSet<Board> mVisitedStates = new HashSet<>();

    public AStarSearch(int boardEdge) {
        int goalCounter = 0;

        mGoalState = new int[boardEdge][boardEdge];

        for(int r = 0; mGoalState.length > r; ++r) {
            for(int c = 0; mGoalState[0].length > c; ++c) {
                mGoalState[r][c] = goalCounter;

                ++goalCounter;
            }
        }
    }

    public ArrayList<Board> search(Board initialState) {
        ArrayList<Board> moveSequence = new ArrayList<>();

        PriorityQueue<Board> statePriorityQueue = new PriorityQueue<>();
        Board exploringState = initialState;

        while(!Arrays.deepEquals(exploringState.getBoard(), mGoalState)) {
            //int hash = exploringState.hashCode();

            if(!mVisitedStates.contains(exploringState)){
                mVisitedStates.add(exploringState);

                for(Board b : exploringState.getAvailableActions()) {
                    statePriorityQueue.add(b);
                }

                moveSequence.add(exploringState);
            }

            /*if(statePriorityQueue.size() == 1){
                System.out.println("uhoh");
            } else if (hash == 950){
                System.out.println("ok");
            }*/

            exploringState = statePriorityQueue.poll();

            if(exploringState == null) {
                return null;
            }
        }

        moveSequence.add(exploringState);

        return moveSequence;
    }

}
