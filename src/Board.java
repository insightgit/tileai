import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Queue;

public class Board implements Comparable<Board> {
    public class Position {
        int row;
        int column;

        Position(int row, int column){
            this.row = row;
            this.column = column;
        }
    }

    private int[][] mBoardArray;
    private int mBoardEdge;
    private Position mZeroPosition;
    private int mCost;

    public Board(String[] args, int cost) {
        Queue<String> stringQueue = new ArrayDeque<>(Arrays.asList(args));

        mBoardEdge = (int)Math.sqrt(args.length);
        mCost = cost;

        mBoardArray = new int[mBoardEdge][mBoardEdge];

        for(int r = 0; mBoardEdge > r; ++r) {
            for (int c = 0; mBoardEdge > c; ++c) {
                mBoardArray[r][c] = Integer.parseInt(stringQueue.remove());

                if(mBoardArray[r][c] == 0){
                    mZeroPosition = new Position(r, c);
                }
            }
        }

        assert mZeroPosition != null;
    }

    public Board(String[] args) {
        this(args, 0);
    }

    private Board(Board board){
        mBoardArray = new int[board.mBoardArray.length]
                             [board.mBoardArray[0].length];

        for(int r = 0; board.mBoardArray.length > r; ++r) {
            for(int c = 0; board.mBoardArray[0].length > c; ++c) {
                mBoardArray[r][c] = board.mBoardArray[r][c];
            }
        }

        mBoardEdge = board.mBoardEdge;

        computeZeroPosition();
    }

    private void computeZeroPosition() {
        for(int r = 0; mBoardEdge > r; ++r) {
            for(int c = 0; mBoardEdge > c; ++c) {
                if(mBoardArray[r][c] == 0){
                    mZeroPosition = new Position(r, c);
                    break;
                }
            }
            if(mZeroPosition != null) {
                break;
            }
        }
    }

    private int getHeuristic() {
        int returnValue = 0;

        for(int r = 0; mBoardArray.length > r; ++r) {
            for(int c = 0; mBoardArray[0].length > c; ++c) {
                int intendedPosSum = mBoardArray[r][c];
                int posSum = (r * 4) + c;

                returnValue += Math.abs(intendedPosSum - posSum);
            }
        }

        return returnValue;
    }

    private Board swap(Position x, Position y){
        Board returnValue = new Board(this);

        int original = returnValue.mBoardArray[x.row][x.column];

        returnValue.mBoardArray[x.row][x.column] = returnValue.mBoardArray[y.row][y.column];
        returnValue.mBoardArray[y.row][y.column] = original;

        returnValue.computeZeroPosition();

        returnValue.mCost = mCost + 1;

        return returnValue;
    }

    public ArrayList<Board> getAvailableActions() {
        ArrayList<Board> returnValue = new ArrayList<>(4);

        if(mZeroPosition.column + 1 < mBoardEdge) {
            returnValue.add(swap(mZeroPosition, new Position(
                            mZeroPosition.row,
                            mZeroPosition.column + 1)));
        }

        if(mZeroPosition.column - 1 >= 0) {
            returnValue.add(swap(mZeroPosition, new Position(
                    mZeroPosition.row,
                    mZeroPosition.column - 1)));
        }

        if(mZeroPosition.row + 1 < mBoardEdge) {
            returnValue.add(swap(mZeroPosition, new Position(
                    mZeroPosition.row + 1,
                    mZeroPosition.column)));
        }

        if(mZeroPosition.row - 1 >= 0) {
            returnValue.add(swap(mZeroPosition, new Position(
                    mZeroPosition.row - 1,
                    mZeroPosition.column)));
        }

        return returnValue;
    }

    public int[][] getBoard() {
        return mBoardArray;
    }

    @Override
    public int compareTo(Board board) {
        int thisSum = mCost + getHeuristic();
        int otherSum = board.mCost + board.getHeuristic();

        return thisSum - otherSum;
    }

    @Override
    public int hashCode() {
        int returnValue = 0;

        for(int r = 0; mBoardArray.length > r; ++r) {
            for(int c = 0; mBoardArray.length > c; ++c) {
                returnValue += mBoardArray[r][c] * ((r * 4) + c);
            }
        }

        return returnValue;
    }

    @Override
    public String toString() {
        String returnValue = "";

        for(int[] row : mBoardArray) {
            for(int column : row) {
                returnValue += column + " ";
            }
            returnValue += "\n";
        }

        return returnValue;
    }
}
